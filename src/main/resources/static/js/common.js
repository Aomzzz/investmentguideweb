
function maxLengthCheck(object) {
    if (object.value.length > object.max.length)
      object.value = object.value.slice(0, object.max.length)
}
    
function roundDecimal(object){
	var decimal		= object.value -Math.floor(object.value);
	decimal			= Number(decimal).toFixed(2);
	var result 		= Math.floor(object.value) + parseFloat(decimal);
	object.value	= result;
}

