package com.aom.investCalculator.services;

import java.text.SimpleDateFormat;

public abstract class AbstractInvestmentConstant {

	public static final double TAX_DEPOSIT 						= 0.15;
	
	public static final int DAYS_PERYEAR						= 365;
	
	public static final int MONTH_PERINTEREST					= 1;
	
	public static final SimpleDateFormat DATE_FORMAT_DEPOSIT	= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

}
