package com.aom.investCalculator.services.model;

import java.util.Calendar;

public class DepositProfitTimeModel {

	private Calendar date;
	
	private double contribAmount;
	
	private double compoundInterest;
	
	public DepositProfitTimeModel() {
	}

	public Calendar getDate() {
		return date;
	}

	public void setDate(Calendar date) {
		this.date = date;
	}

	public double getContribAmount() {
		return contribAmount;
	}

	public void setContribAmount(double contribAmount) {
		this.contribAmount = contribAmount;
	}

	public double getCompoundInterest() {
		return compoundInterest;
	}

	public void setCompoundInterest(double compoundInterest) {
		this.compoundInterest = compoundInterest;
	}

	@Override
	public String toString() {
		return "DepositProfitTimeModel [date=" + date + ", contribAmount="
				+ contribAmount + ", compoundInterest=" + compoundInterest
				+ "]";
	}

	
}
