package com.aom.investCalculator.services.utils;

import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;

public class DaysConverterUtil {
	
	public static int getDaysForDepositMonths(Calendar startCal, int months){
		ZonedDateTime zoneDt	= ZonedDateTime.ofInstant(startCal.toInstant(), ZoneId.systemDefault());		
		LocalDate startDay		= zoneDt.toLocalDate();
		return getDaysForDepositMonths(startDay,months);
	}
	
	public static int getDaysForDepositMonths(int months){
		LocalDate startDay		= LocalDate.now();
		return getDaysForDepositMonths(startDay,months);
	}
	
	public static int getDaysForDepositMonths(LocalDate startDay,int months){
		LocalDate endDay		= startDay.plusMonths(months);
		Period period			= Period.between(startDay, endDay);
		int days				= (int) ChronoUnit.DAYS.between(startDay,endDay);
		System.out.println("Deposit Period : Start "+startDay+", End "+endDay+", Days "+days);
		return days;
	}
}
