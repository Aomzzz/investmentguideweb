package com.aom.investCalculator.services;

import java.util.Calendar;
import java.util.List;

import com.aom.investCalculator.services.exception.InvestmentCalcException;
import com.aom.investCalculator.services.model.DepositProfitTimeModel;


public interface DepositCalcService {

	public List<DepositProfitTimeModel> calculateProfit(Calendar startCal,double startBalance, double annualRate, double montlyAdd,int monthPeriod) throws InvestmentCalcException;


}
