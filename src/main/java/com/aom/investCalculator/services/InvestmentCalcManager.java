package com.aom.investCalculator.services;

import com.aom.investCalculator.application.model.WSDepositCalResponseModel;
import com.aom.investCalculator.services.exception.InvestmentCalcException;

public interface InvestmentCalcManager {

	public WSDepositCalResponseModel calculateDepositInvestment(double startBalance,double annualRate, double montlyAdd, int monthPeriod) throws InvestmentCalcException;

}
