package com.aom.investCalculator.services.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.aom.investCalculator.services.AbstractInvestmentConstant;
import com.aom.investCalculator.services.DepositCalcService;
import com.aom.investCalculator.services.exception.InvestmentCalcException;
import com.aom.investCalculator.services.model.DepositProfitTimeModel;
import com.aom.investCalculator.services.utils.DaysConverterUtil;

@Service(value="FixedAccDepositCalServiceImpl")
public class FixedAccDepositCalServiceImpl extends AbstractInvestmentConstant implements DepositCalcService{
	
	private Logger logger	= Logger.getLogger(this.getClass());
	
	@Override
	public List<DepositProfitTimeModel> calculateProfit(Calendar startCal,double startBalance, double annualRate, double montlyAdd, int monthPeriod) throws InvestmentCalcException{
		Calendar mStartCal		= (Calendar)startCal.clone();
		double monthBalance 	= startBalance;
		double rate				= ( annualRate*(1-TAX_DEPOSIT) )/100.00;
				
		List<DepositProfitTimeModel> lstDepositProfitTime	= new ArrayList<DepositProfitTimeModel>();

		try{
			//**** Calculate DepositProfit at the end of each month ****//
			for(int count=0; count< monthPeriod; count+=MONTH_PERINTEREST ){
				int daysInMonth			= DaysConverterUtil.getDaysForDepositMonths(mStartCal, MONTH_PERINTEREST);
				
				mStartCal.add(Calendar.DATE, daysInMonth);
				
				double monthInterest	= (monthBalance*rate*daysInMonth)/DAYS_PERYEAR;
				monthBalance			+= monthInterest + montlyAdd;
				
				DepositProfitTimeModel monthDepositProfit	= new DepositProfitTimeModel();
				monthDepositProfit.setDate( (Calendar)mStartCal.clone() );
				monthDepositProfit.setCompoundInterest( monthInterest );
				monthDepositProfit.setContribAmount( montlyAdd );
				lstDepositProfitTime.add( monthDepositProfit );
				
			}
			
		} catch(Exception e){
			logger.error(e,e);
			throw new InvestmentCalcException( "FixedAccDepositCalc ERROR : "+e.getMessage() );
		}
		return lstDepositProfitTime;
	}

}
