package com.aom.investCalculator.services.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.aom.investCalculator.application.model.WSDepositCalProfitTimeModel;
import com.aom.investCalculator.application.model.WSDepositCalResponseModel;
import com.aom.investCalculator.services.AbstractInvestmentConstant;
import com.aom.investCalculator.services.DepositCalcService;
import com.aom.investCalculator.services.InvestmentCalcManager;
import com.aom.investCalculator.services.exception.InvestmentCalcException;
import com.aom.investCalculator.services.model.DepositProfitTimeModel;

@Service(value="InvestmentCalcManagerImpl")
public class InvestmentCalcManagerImpl implements InvestmentCalcManager {
	
	@Resource(name="FixedAccDepositCalServiceImpl")
	private DepositCalcService depositCalcService;

	@Override
	public WSDepositCalResponseModel calculateDepositInvestment(double startBalance, double annualRate, double montlyAdd, int monthPeriod) throws InvestmentCalcException{
				
		WSDepositCalResponseModel wsDeptResponseModel			= new WSDepositCalResponseModel();
		
		List<DepositProfitTimeModel> lstProfitTimeModels		= depositCalcService.calculateProfit(Calendar.getInstance(),
																			startBalance, annualRate, montlyAdd, monthPeriod);
		
		List<WSDepositCalProfitTimeModel> tempLstProfitTimeModels= new ArrayList<WSDepositCalProfitTimeModel>();
		
		if(lstProfitTimeModels != null && lstProfitTimeModels.size() >0){
			
			tempLstProfitTimeModels								= accumulateToWSProfitTimeCalcModels(lstProfitTimeModels);
			
			WSDepositCalProfitTimeModel[] wsProfitTimeModels	= new WSDepositCalProfitTimeModel[ tempLstProfitTimeModels.size() ];
			tempLstProfitTimeModels.toArray( wsProfitTimeModels );			
			int lastMonthIndex									= tempLstProfitTimeModels.size() - 1;
			
			wsDeptResponseModel.setWsDepositProfitTimeModels(wsProfitTimeModels);
			wsDeptResponseModel.setTotalContribution( wsProfitTimeModels[lastMonthIndex].getAccumulateContrib() );
			wsDeptResponseModel.setTotalInterest( wsProfitTimeModels[lastMonthIndex].getAccumulateInterest() );
		}
		
		return wsDeptResponseModel;
	}
	
	private List<WSDepositCalProfitTimeModel> accumulateToWSProfitTimeCalcModels( List<DepositProfitTimeModel> lstProfitTimeModels ){ 
		
		List<WSDepositCalProfitTimeModel> accumulateLstProfitModels= new ArrayList<WSDepositCalProfitTimeModel>();
		
		double accumulateContrib								= 0;
		double accumulateInterest								= 0;
		
		for(int i=0; i< lstProfitTimeModels.size(); i++){
			DepositProfitTimeModel srcModel					= lstProfitTimeModels.get(i);
			
			if( srcModel != null ){				
				String date					= AbstractInvestmentConstant.DATE_FORMAT_DEPOSIT.format( srcModel.getDate().getTime() );
				accumulateContrib			+= srcModel.getContribAmount();
				accumulateInterest			+= srcModel.getCompoundInterest();
				
				WSDepositCalProfitTimeModel destModel	= new WSDepositCalProfitTimeModel();
				destModel.setDate(date);
				destModel.setAccumulateContrib(accumulateContrib);
				destModel.setAccumulateInterest(accumulateInterest);
				
				accumulateLstProfitModels.add(destModel);
			}
		}
		return accumulateLstProfitModels;
	}
		
}
