package com.aom.investCalculator.services.exception;

public class InvestmentCalcException extends Exception {
	
	private String errorMessage;

	public InvestmentCalcException(String errorMessage) {
		super(errorMessage);
		this.errorMessage = errorMessage;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	
	
}
