package com.aom.investCalculator.test;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.aom.investCalculator.application.model.WSDepositCalProfitTimeModel;
import com.aom.investCalculator.application.model.WSDepositCalResponseModel;

public class InvestmentCalcControllerTest {
//	public static void main(String[] args) {
//		WSDepositCalResponseModel mockResponseModel	 	= mockSuccessResponseModel();
//		System.out.println(mockResponseModel);
//	}

	public static WSDepositCalResponseModel mockSuccessResponseModel(){
		SimpleDateFormat sdf							 	 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		WSDepositCalResponseModel wsResponseModel		     = new WSDepositCalResponseModel();
		List<WSDepositCalProfitTimeModel> lstProfitTimeModels = new ArrayList<WSDepositCalProfitTimeModel>();
		
		for(int i=0; i<5; i++){
			WSDepositCalProfitTimeModel wsProfitTimeModel        = new WSDepositCalProfitTimeModel();
			Calendar today				= Calendar.getInstance();
			today.add(Calendar.DATE, i*30);
			wsProfitTimeModel.setDate( sdf.format(today.getTime() ) );
			System.out.println(wsProfitTimeModel.getDate());
			wsProfitTimeModel.setAccumulateInterest(i*Math.pow(1.2,2.0));
			wsProfitTimeModel.setAccumulateContrib(i*500);
			lstProfitTimeModels.add(wsProfitTimeModel);
		}
		WSDepositCalProfitTimeModel[] wsLstProfitTimeModels	 = new WSDepositCalProfitTimeModel[lstProfitTimeModels.size()];
		lstProfitTimeModels.toArray(wsLstProfitTimeModels);
		wsResponseModel.setCode(100);
		wsResponseModel.setTotalContribution( wsLstProfitTimeModels[wsLstProfitTimeModels.length-1].getAccumulateContrib() );
		wsResponseModel.setTotalInterest( wsLstProfitTimeModels[wsLstProfitTimeModels.length-1].getAccumulateInterest() );
		wsResponseModel.setWsDepositProfitTimeModels( wsLstProfitTimeModels );
		return wsResponseModel;
	}
}
