package com.aom.investCalculator.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.aom.investCalculator.services.DepositCalcService;
import com.aom.investCalculator.services.impl.FixedAccDepositCalServiceImpl;
import com.aom.investCalculator.services.model.DepositProfitTimeModel;


public class DepositCalculateServiceTest {

	private DepositCalcService depositCalService;
		
	@Before
	public void setUp() {
		depositCalService 		= new FixedAccDepositCalServiceImpl();
	}
	
	
	@Test
	public void testCalculate(){
		SimpleDateFormat sdf		= new SimpleDateFormat("yyyy-MM-dd");
		Calendar today				= new GregorianCalendar().getInstance(Locale.ENGLISH);
		try {
			double monthlyDeposit								= 100;
			double accumulatedContrib							= 0;
			List<DepositProfitTimeModel> lstProfitTimeModels	= depositCalService.calculateProfit(today, 1000, 3.0, monthlyDeposit, 12);
			assertNotNull(lstProfitTimeModels);
			
			System.out.println(sdf.format(today.getTime()) );
			
			for(DepositProfitTimeModel item : lstProfitTimeModels){
				
				today.add(Calendar.MONTH, 1);
				Calendar nextMonth	= (Calendar)today.clone();
				assertEquals( sdf.format(nextMonth.getTime()), sdf.format(item.getDate().getTime()) );
				
				accumulatedContrib		+= monthlyDeposit;
				assertEquals(accumulatedContrib , item.getContribAmount(),0.0);
						
				System.out.println( "Date : "+ item.getDate().getTime() );
				System.out.println( "DepAmount : "+ item.getContribAmount() );
				System.out.println( "Interest : "+item.getCompoundInterest() );
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testCalculateNull(){
		Calendar today											= Calendar.getInstance();
		try {
			List<DepositProfitTimeModel> lstProfitTimeModels	= depositCalService.calculateProfit(today, 0, 0.0, 0, 0);
			assertNotNull(lstProfitTimeModels);
			assertEquals( 0, lstProfitTimeModels.size() );
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@After
	public void tearDown() {
		
	}
}
