package com.aom.investCalculator.test;

import java.util.Calendar;

import org.junit.Test;

import com.aom.investCalculator.services.utils.DaysConverterUtil;

public class DaysConverterUtilTest {

//	@Test
	public void testGetDaysForDepositMonthsFromToday(){
		int days		= DaysConverterUtil.getDaysForDepositMonths(4);
		System.out.println(days);
	}
	
	@Test
	public void testGetDaysForDepositMonthsFromDay(){
		Calendar cal 	= Calendar.getInstance();
		cal.set(2015, 1, 2);
		int days		= DaysConverterUtil.getDaysForDepositMonths(cal,1);
		System.out.println(days);
	}
	
}
