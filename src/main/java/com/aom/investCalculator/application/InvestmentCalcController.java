package com.aom.investCalculator.application;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.aom.investCalculator.application.model.WSDepositCalRequestModel;
import com.aom.investCalculator.application.model.WSDepositCalResponseModel;
import com.aom.investCalculator.services.InvestmentCalcManager;

@RestController
public class InvestmentCalcController extends AbstractInvestmentGuideWeb {
	
	private Logger logger		= Logger.getLogger(this.getClass());
	
	@Resource(name="InvestmentCalcManagerImpl")
	private InvestmentCalcManager investmentCalcManager;
	
	@RequestMapping(value="/deposit-calculate.json", method=RequestMethod.POST)
	public WSDepositCalResponseModel depositCalculator( @RequestBody WSDepositCalRequestModel requestModel ){
		logger.info("Call /deposit-calculate.json" + requestModel.toString());

		WSDepositCalResponseModel responseModel		= new WSDepositCalResponseModel();

		String errorMessage							= null;
	
		try{
			
//			TODO Validate input data
//			responseModel	= InvestmentCalcControllerTest.mockSuccessResponseModel();
			responseModel	= investmentCalcManager.calculateDepositInvestment(requestModel.getStartBalance(), 
					requestModel.getAnnualRate(), requestModel.getMonthlyAddition(), requestModel.getMonthPeriod());
			
			responseModel.setCode(CODE_SUCCESS);
			
		}
		catch(Exception e){
			logger.error(e,e);
			errorMessage							= e.getMessage();
			responseModel.setErrorMessage(MSG_ERROR +" : "+errorMessage);
			
			responseModel.setCode(CODE_ERROR);
		}
		return responseModel;
	}
	
	
	private void validateWSDepositCalRequestModel(WSDepositCalRequestModel requestModel){
		
	}
	
	
}
