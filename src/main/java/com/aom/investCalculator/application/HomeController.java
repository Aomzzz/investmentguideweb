package com.aom.investCalculator.application;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HomeController {
	
	private Logger logger	= Logger.getLogger(this.getClass());
	
	@RequestMapping(value="/")
	public String rootContext(){
		return "redirect:home.htm";
	}
	
	@RequestMapping(value="/home.htm")
	public String home(){
		logger.info("==== Call /home ====");
		return "home";
	}
	
	@RequestMapping(value="/contact.htm")
	public String contact(){
		return "contact";
	}
}
