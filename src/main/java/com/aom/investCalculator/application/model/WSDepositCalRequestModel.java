package com.aom.investCalculator.application.model;

import java.util.Calendar;

public class WSDepositCalRequestModel {
	
	private double startBalance;
	
	private double annualRate;
	
	private double monthlyAddition;
	
	private int monthPeriod;

	public double getStartBalance() {
		return startBalance;
	}

	public void setStartBalance(double startBalance) {
		this.startBalance = startBalance;
	}

	public double getAnnualRate() {
		return annualRate;
	}

	public void setAnnualRate(double annualRate) {
		this.annualRate = annualRate;
	}

	public double getMonthlyAddition() {
		return monthlyAddition;
	}

	public void setMonthlyAddition(double monthlyAddition) {
		this.monthlyAddition = monthlyAddition;
	}

	public int getMonthPeriod() {
		return monthPeriod;
	}

	public void setMonthPeriod(int monthPeriod) {
		this.monthPeriod = monthPeriod;
	}

	@Override
	public String toString() {
		return "WSDepositCalRequestModel [startBalance=" + startBalance
				+ ", annualRate=" + annualRate + ", monthlyAddition="
				+ monthlyAddition + ", monthPeriod=" + monthPeriod + "]";
	}
	
	
}
