package com.aom.investCalculator.application.model;


public class WSDepositCalProfitTimeModel {

	private String date;
	
	private double accumulateContrib;
	
	private double accumulateInterest;
	

	public WSDepositCalProfitTimeModel() {
		super();
	}

	public WSDepositCalProfitTimeModel(String date, double accumulateContrib,
			double accumulateInterest) {
		super();
		this.date = date;
		this.accumulateContrib = accumulateContrib;
		this.accumulateInterest = accumulateInterest;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public double getAccumulateContrib() {
		return accumulateContrib;
	}

	public void setAccumulateContrib(double accumulateContrib) {
		this.accumulateContrib = accumulateContrib;
	}

	public double getAccumulateInterest() {
		return accumulateInterest;
	}

	public void setAccumulateInterest(double accumulateInterest) {
		this.accumulateInterest = accumulateInterest;
	}

	@Override
	public String toString() {
		return "WSDepositCalProfitTimeModel [date=" + date
				+ ", accumulateContrib=" + accumulateContrib
				+ ", accumulateInterest=" + accumulateInterest + "]";
	}

}
