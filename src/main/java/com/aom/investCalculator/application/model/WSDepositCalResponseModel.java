package com.aom.investCalculator.application.model;

import java.util.Arrays;

public class WSDepositCalResponseModel {

	private int code;
	
	private String errorMessage;

	private double totalInterest;
	
	private double totalContribution;
	
	private WSDepositCalProfitTimeModel[] wsDepositProfitTimeModels;

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public double getTotalInterest() {
		return totalInterest;
	}

	public void setTotalInterest(double totalInterest) {
		this.totalInterest = totalInterest;
	}

	public double getTotalContribution() {
		return totalContribution;
	}

	public void setTotalContribution(double totalContribution) {
		this.totalContribution = totalContribution;
	}

	public WSDepositCalProfitTimeModel[] getWsDepositProfitTimeModels() {
		return wsDepositProfitTimeModels;
	}

	public void setWsDepositProfitTimeModels(
			WSDepositCalProfitTimeModel[] wsDepositProfitTimeModels) {
		this.wsDepositProfitTimeModels = wsDepositProfitTimeModels;
	}

	@Override
	public String toString() {
		return "WSDepositCalResponseModel [code=" + code + ", errorMessage="
				+ errorMessage + ", totalInterest=" + totalInterest
				+ ", totalContribution=" + totalContribution
				+ ", wsDepositProfitTimeModels="
				+ Arrays.toString(wsDepositProfitTimeModels) + "]";
	}	
	
	
}
