package com.aom.investCalculator.application;

public abstract class AbstractInvestmentGuideWeb {

	protected final static int CODE_SUCCESS		= 100;
	
	protected final static int CODE_ERROR		= 900;
	
	protected final static String MSG_SUCCESS	= "SUCCESS";
	
	protected final static String MSG_ERROR		= "ERROR";
}
