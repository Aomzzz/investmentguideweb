
var mainApp 			= mainApp || {};

mainApp.CONFIG_BANK	= {
	ANNUAL_RATE			: 6,
	MONTH_PERIOD		: 60
};

mainApp.plotModel	= {
		date			 	: [],
		accumulateContrib	: [],
		accumulateInterest	: []
};

mainApp.PATH  = {
  DEPOSIT_CALCULATE     : "/deposit-calculate.json"
};

mainApp.setContextPath 	= function(contextPath){
	var mContextPath	   = contextPath.substr(0,contextPath.length-1);
	for(var key in mainApp.PATH){
		mainApp.PATH[key]  = mContextPath + mainApp.PATH[key];
	}
}

mainApp.init 			= function(contextPath){
	console.log("Context Path : "+contextPath);
	mainApp.setContextPath(contextPath);
	mainApp.initFunctionButton();
	mainApp.plotInvestSummary( mainApp.plotModel ); //Plot Empty Graph
}

mainApp.initFunctionButton  = function(){
	$("#btnSubmitCalc").bind("click",function(){
		mainApp.submitCalculateDeposit();
	});
}

mainApp.submitCalculateDeposit  = function(){
	var requestParams	= mainApp.getDepositCalcRequest();
	var isEmptyFields	= mainApp.isNullInput(requestParams);
	if(isEmptyFields){
		return;
	}
	
	var isValidate		= mainApp.isInputValidate(requestParams);
	
	if( isValidate ){
		
		$.ajax( {
			url			: mainApp.PATH.DEPOSIT_CALCULATE,
			method		: "POST",
			contentType	: "application/json; charset=UTF-8",
			data		: JSON.stringify(requestParams),
			beforeSend	: function(){
			}
		}).done(function( responseModel ) {
			console.log( responseModel );
			
			mainApp.clear();
			if( responseModel != null && responseModel.code != null && responseModel.code == 100 ){
				mainApp.plotInvestSummary( responseModel.wsDepositProfitTimeModels );
				mainApp.showReportSummary( responseModel.totalInterest, responseModel.totalContribution );
			} else{
				var msg  = "Code " + responseModel.code + " : "+responseModel.errorMessage;
				console.log( msg );
				alert( msg )
			}
		}).fail(function() {
		    alert( "Fatal Error : Please contact Administrator" );
		});
		
	}
}


function validateTwoPrecision(value){
	var regex		= /(^\d*)(\.?\d{0,2})$/;
	return regex.test(value );
}
function validateInteger(value){
	var regex		= /(^\d*)$/;
	return regex.test(value );
}

mainApp.isInputValidate		= function(inputObj){
	if( validateTwoPrecision(inputObj.startBalance)  == false ){
		alert("Please fill in StartBalance only Two decimal point");
		return false;
	}
	
	if( validateTwoPrecision(inputObj.annualRate) == false ){
		alert("Please fill in AnnualRate only Two decimal point");
		return false;
	} else if( parseFloat(inputObj.annualRate) > mainApp.CONFIG_BANK.ANNUAL_RATE ){
		alert("Sorry, Thailand Bank haven't provide the promotion for interest rate more than "+ mainApp.CONFIG_BANK.ANNUAL_RATE +"%");
		return false;
	}

	if( validateTwoPrecision(inputObj.monthlyAddition) == false ){
		alert("Please fill in MonthlyAddition only Two decimal point");
		return false;
	}
	
	if( validateInteger(inputObj.monthPeriod) == false ){
		alert("Please fill in MonthPeriod by integer number");
		return false;
	} else if( parseInt(inputObj.monthPeriod) > mainApp.CONFIG_BANK.MONTH_PERIOD ){
		alert("Sorry, Thailand Bank haven't provide the promotion for deposit period longer than "+ mainApp.CONFIG_BANK.MONTH_PERIOD +" years");
		return false;
	}
	return true;
}

mainApp.isNullInput			= function(inputObj){
	for(key in inputObj){
		if(inputObj[key] == null || inputObj[key] == "" ){
			alert("Plese fill in all required input");
			return true;
		}
	}
	return false;
}

mainApp.getDepositCalcRequest = function(){
	var params    = {
	     startBalance    : $("#inputStartBalance").val().trim(),
	     annualRate      : $("#inputAnnualRate").val().trim(),
	     monthlyAddition : $("#inputMonthlyAdd").val().trim(),
	     monthPeriod     : $("#inputMonthPeriod").val().trim()
	}
	return params;
}

mainApp.showReportSummary	= function( totalInterest, totalContrib ){
	var totalInterest		= parseFloat(totalInterest).toFixed(2);
	var totalContrib 		= parseFloat(totalContrib).toFixed(2);
	$("#report-summary .totalInterest").text( totalInterest +" Bahts");
	$("#report-summary .totalContrib").text( totalContrib   +" Bahts");
}

/****Map Data to Formated Plot data {date, accumulateContrib, accumulateInterest}*****/
mainApp.mapToDepositPlotModel		= function(wsProfitTimeModels){
	
	if( wsProfitTimeModels != null && wsProfitTimeModels.length > 0 ){
		
		wsProfitTimeModels.forEach(function(element, index, array){			
			if( element != null ){
				mainApp.plotModel.date.push( element.date );
				mainApp.plotModel.accumulateContrib.push( element.accumulateContrib );
				mainApp.plotModel.accumulateInterest.push( element.accumulateInterest );
			}			
		});		
	}
}

mainApp.plotInvestSummary	= function( wsDepositProfitTimeModels ){
	mainApp.mapToDepositPlotModel(wsDepositProfitTimeModels);
	
	var traceDeposit = {
		x		: mainApp.plotModel.date,
		y		: mainApp.plotModel.accumulateContrib,
		name	: 'Contributions',
		type	: 'scatter'
	};

	var traceInterest = {
		x		:  mainApp.plotModel.date,
		y		:  mainApp.plotModel.accumulateInterest,
		name	: 'CompoundInterest',
		yaxis	: 'y2',
		type	: 'scatter'
	};

	var data	= [ traceDeposit, traceInterest ];

	var layout	= {
		title 			: 'Accumulate Interest earned and Contributions Over Time',
		yaxis			: {
			title		: 'Contributions (Bahts)'
		},
		yaxis2 : {
			title 		: 'CompoundInterest (%)',
			overlaying	: 'y',
			side		: 'right'
		},
		autosize		: false
	};

	console.log("Plot Data : "+data);
	var plotDiv		= document.getElementById("invest-summary").getElementsByClassName("plotDiv")[0];
	Plotly.newPlot( plotDiv,data,layout, {displayModeBar: false} );
}


mainApp.clear 		= function(){
	mainApp.plotModel.date.length				= 0;
	mainApp.plotModel.accumulateContrib.length	= 0;
	mainApp.plotModel.accumulateInterest.length	= 0;
}


var mockData	= {"code":100,"errorMessage":null,"totalInterest":0.0,
"totalDeposit":0.0,"wsDepositProfitTimeModels":
[{"date":"2016-05-22","accumulateContrib":0.0,"accumulateInterest":0.0},
	{"date":"2016-06-21","accumulateContrib":500.0,"accumulateInterest":1.2},
	{"date":"2016-07-21","accumulateContrib":1000.0,"accumulateInterest":2.4},
	{"date":"2016-08-20","accumulateContrib":1500.0,"accumulateInterest":3.5999999999999996},
	{"date":"2016-09-19","accumulateContrib":2000.0,"accumulateInterest":4.8}]};